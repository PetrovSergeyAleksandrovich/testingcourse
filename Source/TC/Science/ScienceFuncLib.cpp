// Fill out your copyright notice in the Description page of Project Settings.


#include "Science/ScienceFuncLib.h"

DEFINE_LOG_CATEGORY_STATIC(LogScience, All, All)

int32 UScienceFuncLib::Fibonacci(int32 InValue)
{
	//check(InValue >= 0);

	if (InValue < 0)
	{
		UE_LOG(LogScience, Error, TEXT("Invalid input for Fibonacci : %i"), InValue);
	}

	return InValue <= 1 ? InValue : Fibonacci(InValue - 1) + Fibonacci(InValue - 2);
}
