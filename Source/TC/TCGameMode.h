// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TCGameMode.generated.h"

UCLASS(minimalapi)
class ATCGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATCGameMode();
};



