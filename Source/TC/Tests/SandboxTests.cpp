// Fill out your copyright notice in the Description page of Project Settings.


#if (WITH_DEV_AUTOMATION_TESTS || WITH_PERF_AUTOMATION_TESTS) //use this shit, if you need to remove test from Shipping configuration

#include "SandboxTests.h"
#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "Math/UnrealMathUtility.h"
#include "TestUtils.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMathMaxInt, "TCGame.Math.MaxInt",
								 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter | EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMathSqrt, "TCGame.Math.Sqrt",
								 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
									 EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMathRandom, "TCGame.Math.Random",
								 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
									 EAutomationTestFlags::HighPriority);

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FMathSin, "TCGame.Math.Sin",
								 EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
									 EAutomationTestFlags::HighPriority);


bool FMathMaxInt::RunTest(const FString& Parameters)
{
	TestTrue("2 different positive numbers", FMath::Max(13, 25) == 25);

	return true;
}

bool FMathSqrt::RunTest(const FString& Parameters)
{
	TestEqual("Sqrt from 25", FMath::Sqrt(25.0f), 5.0f);

	return true;
}

bool FMathRandom::RunTest(const FString& Parameters)
{
	for (int32 i = 0; i < 3; i++)
	{
		auto x = FMath::RandHelper(2);
		auto y = FMath::RandHelper(2);
		FString output = "x";
		bool success = TestEqual(output, x, y);
		if (success)
		{
			AddInfo("Numbers are equal");
		}
		else
		{
			AddInfo("Numbers are not equal");
		}	
	}

	return true;
}

bool FMathSin::RunTest(const FString& Parameters)
{
	AddInfo("Sin func testing");

	typedef float Degrees;
	typedef TArray<TestPayload<Degrees, float>> SinTestPayload;
	// clang-format off
    const SinTestPayload TestData
    {
        {Degrees{0.00f}, 0.0f},
        {Degrees{30.0f}, 0.5f},
        {Degrees{45.0f}, 0.707f},
        {Degrees{60.0f}, 0.866f},
        {Degrees{90.0f}, 1.0f},
    };
	// clang-format on

	for (const auto Data : TestData)
	{
		const float Rad = FMath::DegreesToRadians(Data.TestValue);
		TestTrueExpr(FMath::IsNearlyEqual(FMath::Sin(Rad), Data.ExpectedValue, 0.001f));
	}

	return true;
}
#endif
 