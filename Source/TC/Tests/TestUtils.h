#pragma once

template <typename T1, typename T2>
struct TestPayload
{
	T1 TestValue;
	T2 ExpectedValue;
	float Tolerance = KINDA_SMALL_NUMBER;
};




