// Fill out your copyright notice in the Description page of Project Settings.

#if WITH_AUTOMATION_TESTS

#include "Tests/BatteryTest.h"
#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "Battery.h"


IMPLEMENT_SIMPLE_AUTOMATION_TEST(FBatteryTests, "TCGame.Items.Battery", 
								EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter |
									 EAutomationTestFlags::CriticalPriority);

bool FBatteryTests::RunTest(const FString& Parameters)
{
	AddInfo("Battery with default constructor");
	const TC::Battery BatteryDefault;
	TestTrueExpr(FMath::IsNearlyEqual(BatteryDefault.GetPercent(), 1.0f));
	TestTrueExpr(BatteryDefault.GetColor() == FColor::Green);
	TestTrueExpr(BatteryDefault.ToString().Equals("100%"));


	AddInfo("Battery with custom constructor");
	//LAMBDA FUNCTOR (function as variable)
	const auto BatteryTestFunc = [this](float Percent, FColor Color, const FString& PercentString)
	{
		const TC::Battery BatteryObject(Percent);
		TestTrueExpr(FMath::IsNearlyEqual(BatteryObject.GetPercent(), FMath::Clamp(Percent, 0.0f, 1.0f)));
		TestTrueExpr(BatteryObject.GetColor() == Color);
		TestTrueExpr(BatteryObject.ToString().Equals(PercentString));
	};

	BatteryTestFunc(1.0f, FColor::Green, "100%");
	BatteryTestFunc(0.46f, FColor::Yellow, "46%");
	BatteryTestFunc(0.25f, FColor::Red, "25%");
	BatteryTestFunc(3000.0f, FColor::Green, "100%");
	BatteryTestFunc(-3000.0f, FColor::Green, "0%");

	AddInfo("Battery charge/ uncharge");
	TC::Battery BatteryDefault1{0.6f};
	TestTrueExpr(FMath::IsNearlyEqual(BatteryDefault.GetPercent(), 0.6f));
	BatteryDefault1.Uncharge();
	TestTrueExpr(FMath::IsNearlyEqual(BatteryDefault.GetPercent(), 0.6f - BatteryDefault.ChargeAmount));
	BatteryDefault1.Charge();
	TestTrueExpr(FMath::IsNearlyEqual(BatteryDefault.GetPercent(), 0.6f));

	AddInfo("Battery charge/ uncharge. Corner cases");
	for (int32 i = 0; i < 100; i++)
	{
		BatteryDefault1.Uncharge(); 
	}
	TestTrueExpr(FMath::IsNearlyEqual(BatteryDefault.GetPercent(), 0.5f));

	for (int32 i = 0; i < 100; i++)
	{
		BatteryDefault1.Charge();
	}
	TestTrueExpr(FMath::IsNearlyEqual(BatteryDefault.GetPercent(), 0.6f));


	//Here below we are going to compare 2 different batteries
	AddInfo("Battery comparison");

	TC::Battery BatteryLow(0.3f);
	TC::Battery BatteryHigh(0.9f);

	TestTrueExpr(BatteryHigh >= BatteryLow);
	TestTrueExpr(BatteryHigh == BatteryHigh);

	AddInfo("Battery comparison in memory");
	TestNotSame("Not the same batteries", BatteryLow, BatteryHigh);

	TC::Battery BatteryHighDplicate{0.9f};
	TestTrueExpr(BatteryHigh == BatteryHighDplicate);
	TestNotSame("Not the same batteries", BatteryHighDplicate, BatteryHigh);

	TC::Battery& BatteryHighRef = BatteryHigh;
	TestSame("The same batteries", BatteryHigh, BatteryHighRef);

	return true;
}

#endif
