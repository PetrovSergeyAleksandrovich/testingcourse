// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TC : ModuleRules
{
	public TC(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "Json", "JsonUtilities", "UMG" });

        PublicIncludePaths.AddRange(new string[]{"TC", "TC/Science" , "TC/Tests", "TC/Items"});


    }
}
