// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

namespace TC
{
class TC_API Battery
{
public:
	//public methods

	Battery() = default;

	Battery(float InPercent);

	void Charge();
	void Uncharge();

	float GetPercent() const;
	FColor GetColor() const;

	FString ToString() const;

	bool operator >= (const Battery& rhs) const { return GetPercent() >= rhs.GetPercent(); }

	bool operator == (const Battery& rhs) const { return FMath::IsNearlyEqual(GetPercent(),rhs.GetPercent()); }

private:

	float Percent = 1.0f;

	void SetPercent(float InPercent);

public:
	//public variables

	float ChargeAmount = 0.01f;
};
} // namespace TC