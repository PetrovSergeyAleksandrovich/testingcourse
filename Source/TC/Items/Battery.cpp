// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/Battery.h"

TC::Battery::Battery(float InPercent)
{
	SetPercent(InPercent);
}

void TC::Battery::Charge()
{
	SetPercent(Percent + ChargeAmount);
}

void TC::Battery::Uncharge()
{
	SetPercent(Percent - ChargeAmount);
}

float TC::Battery::GetPercent() const
{
	return Percent;
}

FColor TC::Battery::GetColor() const
{
	if (Percent > 0.8f)
	{
		return FColor::Green;
	}
	else if (Percent > 0.3f)
	{
		return FColor::Yellow;
	}
	else
	{
		return FColor::Red;
	}
}

FString TC::Battery::ToString() const
{
	return FString::Printf(TEXT("%i%%"), FMath::RoundToInt(Percent * 100));
}

void TC::Battery::SetPercent(float InPercent)
{
	Percent = FMath::Clamp(InPercent, 0.0f, 1.0f);
}
