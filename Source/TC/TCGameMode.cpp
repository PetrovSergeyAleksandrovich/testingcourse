// Copyright Epic Games, Inc. All Rights Reserved.

#include "TCGameMode.h"
#include "TCCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATCGameMode::ATCGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

}
